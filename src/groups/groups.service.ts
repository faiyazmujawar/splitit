import { BadRequestException, Inject, Injectable, NotFoundException, Request } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { User } from '@prisma/client';
import { DbClient } from 'src/db/db.client';

@Injectable()
export class GroupsService {
    constructor(@Inject(REQUEST) private request: Request) { }

    async create(name: string) {
        const client = await DbClient.get();
        return await client.group.create({
            data: {
                name,
                members: {
                    connect: [{ id: (this.request['user'] as User).id }]
                }
            },
            select: {
                id: true,
                name: true,
                members: true
            }
        });
    }

    async join(groupId: string) {
        const client = await DbClient.get();
        const group = await client.group.findFirst({ where: { id: groupId } });
        if (group == undefined) throw new NotFoundException(`Group {${groupId}} not found`);
        const user: User = this.request['user'];
        return await client.group.update({
            where: { id: groupId },
            select: { id: true, name: true, members: true },
            data: {
                members: { connect: { id: user.id } }
            }
        });
    }

    async leave(groupId: string) {
        const client = await DbClient.get();
        const group = await client.group.findFirst({
            where: { id: groupId },
            select: {
                id: true,
                members: true,
                name: true
            }
        });
        if (group == undefined) {
            throw new NotFoundException(`Group {${groupId}} not found`);
        }
        const user: User = this.request['user'];
        const isMember = group.members.find(member => member.id === user.id) != undefined;
        if (!isMember) {
            throw new BadRequestException(`Cannot leave a group you're not a member of`);
        }
        return await client.group.update({
            where: { id: group.id },
            select: {
                id: true,
                members: true,
                name: true
            },
            data: {
                members: { disconnect: { id: user.id } }
            }
        });
    }
}
