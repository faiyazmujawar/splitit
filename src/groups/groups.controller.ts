import { Body, Controller, Get, Post, Put, Query, UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/auth/auth.guard';
import { GroupsService } from './groups.service';

@Controller('groups')
@UseGuards(AuthGuard)
export class GroupsController {
    constructor(private groupService: GroupsService) { }

    @Put('/join')
    async join(@Query('id') id: string) {
        return await this.groupService.join(id);
    }

    @Put('/leave')
    async leave(@Query('id') id: string) {
        return await this.groupService.leave(id);
    }

    @Post()
    async create(@Body() data: { name: string; }) {
        return await this.groupService.create(data.name);
    }
}
