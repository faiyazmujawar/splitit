import { Module } from '@nestjs/common';
import { JwtService } from 'src/jwt/jwt.service';
import { GroupsController } from './groups.controller';
import { GroupsService } from './groups.service';

@Module({
  controllers: [GroupsController],
  providers: [GroupsService, JwtService]
})
export class GroupsModule { }
