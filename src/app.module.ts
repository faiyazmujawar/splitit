import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JwtService } from './jwt/jwt.service';
import { AuthModule } from './auth/auth.module';
import { GroupsModule } from './groups/groups.module';

@Module({
  imports: [ConfigModule.forRoot({
    envFilePath: '.env'
  }), AuthModule, GroupsModule],
  controllers: [AppController],
  providers: [AppService, JwtService],
})
export class AppModule { }
