import { Body, Controller, HttpCode, Post } from '@nestjs/common';
import { User } from '@prisma/client';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @Post('/signup')
    @HttpCode(201)
    async signup(@Body() data: User) {
        // TODO: SMS verification
        return await this.authService.signup(data);
    }

    @Post('/login')
    @HttpCode(200)
    async login(@Body() data: { username: string; password: string; }) {
        return await this.authService.login(data);
    }
}
