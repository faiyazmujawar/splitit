import { ForbiddenException, Injectable, NotFoundException } from '@nestjs/common';
import { User } from '@prisma/client';
import { DbClient } from 'src/db/db.client';
import { JwtService } from 'src/jwt/jwt.service';
import { hashSync, compareSync } from "bcrypt";

@Injectable()
export class AuthService {
    constructor(private jwtService: JwtService) { }

    async signup(data: User) {
        const client = await DbClient.get();

        const user = await client.user.create({ data: { ...data, password: hashSync(data.password, 10) } });
        const token = this.jwtService.sign({ userId: user.id });
        return { token };
    }

    async login(data: { username: string; password: string; }) {
        const client = await DbClient.get();

        const user = await client.user.findFirst({ where: { username: data.username } });
        if (user == undefined) {
            throw new NotFoundException(`User ${data.username} not found`);
        }
        if (!compareSync(data.password, user.password)) {
            throw new ForbiddenException('Password incorrect');
        }
        const token = this.jwtService.sign({ userId: user.id });

        return { token };
    }
}
