import { CanActivate, ExecutionContext, ForbiddenException, Injectable, InternalServerErrorException, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { DbClient } from 'src/db/db.client';
import { JwtService } from 'src/jwt/jwt.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private jwtService: JwtService) { }

  canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const token = request.headers['token'];
    if (token == undefined) {
      throw new ForbiddenException('Token is required');
    }

    if (!/Bearer .+/.test(token)) throw new ForbiddenException('Token malformed');
    return DbClient.get()
      .then(async (client) => {
        const user = await client.user.findFirst({ where: { id: this.getUserId(token as string) } });
        if (user == undefined) {
          throw new UnauthorizedException('User not found');
        }
        request['user'] = user;
        return true;
      })
      .catch(error => {
        console.log(error);

        throw new InternalServerErrorException(error);
      });
  }

  getUserId(token: string) {
    const payload = this.jwtService.verify(token.split('Bearer ')[1]);
    return payload['userId'];
  }
}