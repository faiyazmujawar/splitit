import { Module } from '@nestjs/common';
import { JwtService } from 'src/jwt/jwt.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';

@Module({
  controllers: [AuthController],
  providers: [AuthService, JwtService]
})
export class AuthModule { }
