import { PrismaClient } from '@prisma/client';

export class DbClient {
    private static client: PrismaClient | null = null;
    private static isConnected: boolean = false;

    public static async get(): Promise<PrismaClient> {
        if (!this.isConnected) {
            this.client = new PrismaClient();
            await this.client.$connect();
            this.isConnected = true;
        }
        return this.client;
    }
}
