import { User } from "@prisma/client";
import * as Joi from "joi";

export function validateSignup(data: User) {
    const schema = Joi.object<User>({
        firstname: Joi.string().required(),
        lastname: Joi.string().required(),
        username: Joi.string().required().min(5),
        password: Joi.string().required().min(5),
        contact: Joi.number().required(),
    }).options({ abortEarly: false });
    return schema.validate(data);
}